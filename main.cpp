#include <iostream>

#include "ECS.h"

#include <thread>

struct Position : ECSComponent<Position>
{
	Position() = default;
	Position(float x, float y)
		: x(x), y(y)
	{
	}

	float x = 0.0f;
	float y = 0.0f;
};

struct MoveSpeed : ECSComponent<MoveSpeed>
{
	MoveSpeed() = default;
	MoveSpeed(float speedX, float speedY)
		: speedX(speedX), speedY(speedY)
	{
	}

	float speedX = 50.0f;
	float speedY = 20.0f;
};

struct Color : ECSComponent<Color>
{
	Color() = default;
	explicit Color(uint32_t color)
		: color(color)
	{
	}

	uint32_t color = 0xffffffff;
};

std::mutex coutMutex;

struct MovingSystem : ECSSystem<Position, MoveSpeed>
{
	void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components) override
	{
		Position& position = *(Position*)components[0];
		MoveSpeed& speed = *(MoveSpeed*)components[1];

		position.x += speed.speedX * (float)deltaTime;
		position.y += speed.speedY * (float)deltaTime;

		{
			std::lock_guard<std::mutex> guard(coutMutex);
			std::cout<<"Pos x " << position.x << ", y " << position.y << "\n";
		}
	}
};

struct RenderingSystem : ECSSystem<Color>
{
	void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components) override
	{
		Color& color = *(Color*)components[0];

		{
			std::lock_guard<std::mutex> guard(coutMutex);
			std::cout << "Render " << std::hex << color.color << " rectangle\n";
		}
	}
};

int main()
{
	Timestep deltaTime = 1.0;

	ECSSystemList physicsSystems;
	physicsSystems.AddSystem(new MovingSystem);

	ECSSystemList renderingSystems;
	renderingSystems.AddSystem(new RenderingSystem);

	ECS ecs;

	auto player = ecs.CreateEntity();
	ecs.AddComponent<Position>(player, 10.0f, 10.0f);
	ecs.AddComponent<Color>(player, 0xff0000ff);
	ecs.AddComponent<MoveSpeed>(player);

	std::jthread physicsWork([&ecs, &physicsSystems, &deltaTime](){
		while(true){
			ecs.Update(physicsSystems, deltaTime);
		}
	});

	std::jthread renderWork([&ecs, &renderingSystems, &deltaTime](){
		while(true){
			ecs.Update(renderingSystems, deltaTime);
		}
	});

	return 0;
}
