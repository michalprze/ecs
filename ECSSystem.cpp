#include "ECSSystem.h"

void BaseECSSystem::AddComponentType(size_t componentType)
{
	std::lock_guard<std::mutex> guard(componentTypeMutex);
	componentTypes.push_back(componentType);
}

void ECSSystemList::RemoveSystem(BaseECSSystem& system)
{
	std::lock_guard<std::mutex> guard(systemsMutex);
	auto rm = std::remove_if(systems.begin(), systems.end(), [&](const BaseECSSystem* s) {
		return s == &system;
	}); systems.erase(rm, systems.cend());
}
