#pragma once

struct Timestep
{
	Timestep() = default;
	Timestep(double time) : time(time) {}

	operator double() const { return time; }

	[[nodiscard]] double getSeconds() const { return time; }
	[[nodiscard]] double getMilliseconds() const { return time * 1000.0; }

	auto operator <=>(const Timestep&) const = default;

private:
	double time;
};
