#pragma once

#include <vector>
#include <mutex>

#include <cstdio>

#include "Timestep.h"
#include "ECSComponent.h"

//TODO: consider implementing lock-free
struct BaseECSSystem
{
	BaseECSSystem() = default;
	virtual ~BaseECSSystem() = default;

	virtual void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components) = 0;

protected:
	friend struct ECS;

	void AddComponentType(size_t componentType);

	std::mutex componentTypeMutex;
	std::vector<size_t> componentTypes;
};

template<typename ... Args>
struct ECSSystem : BaseECSSystem
{
	ECSSystem()
	{
		(AddComponentType(Args::ID), ...);
	}

	virtual ~ECSSystem() override = default;
};

struct ECSSystemList
{
	ECSSystemList() = default;
	~ECSSystemList()
	{
		std::lock_guard<std::mutex> guard(systemsMutex);
		for(const auto& system : systems) {
			delete system;
		} systems.clear();
	}

	ECSSystemList(const ECSSystemList& other)
	{
		std::lock_guard<std::mutex> guard(systemsMutex);
		systems.resize(other.systems.size());
		//memcpy(systems.data(), other.systems.data(), sizeof(BaseECSSystem) * systems.size());
	}

	ECSSystemList& operator=(const ECSSystemList& other)
	{
		if(this == &other) {
			return *this;
		}

		ECSSystemList temp{ other };

		std::lock_guard<std::mutex> guard(systemsMutex);
		swap(systems, temp.systems);
	}

	ECSSystemList(ECSSystemList&& other) noexcept
			//: systems(std::move(other.systems)) // actualy is there a way to protect initializer list?
	{
		std::lock_guard<std::mutex> guard(systemsMutex);
		systems = std::move(other.systems);
	}

	ECSSystemList& operator=(ECSSystemList&& other) noexcept
	{
		if(this == &other) {
			return *this;
		}

		std::lock_guard<std::mutex> guard(systemsMutex);
		systems = std::move(other.systems);
	}

	inline void AddSystem(BaseECSSystem& system)
	{
		std::lock_guard<std::mutex> guard(systemsMutex);
		systems.push_back(&system);
	}

	inline void AddSystem(BaseECSSystem* system)
	{
		std::lock_guard<std::mutex> guard(systemsMutex);
		systems.push_back(system);
	}

	[[nodiscard]] inline size_t size()
	{
		std::lock_guard<std::mutex> guard(systemsMutex);
		return systems.size();
	}

	[[nodiscard]] inline BaseECSSystem* operator[](size_t index)
	{
		std::lock_guard<std::mutex> guard(systemsMutex);
		return systems[index];
	}

	[[nodiscard]] auto begin()
	{
		std::lock_guard<std::mutex> guard(systemsMutex);
		return systems.begin();
	}

	[[nodiscard]] auto end()
	{
		std::lock_guard<std::mutex> guard(systemsMutex);
		return systems.end();
	}

	void RemoveSystem(BaseECSSystem& system);

private:
	std::mutex systemsMutex;
	std::vector<BaseECSSystem*> systems;
};



