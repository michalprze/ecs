#include "ECS.h"

ECS::~ECS()
{
	{
		std::lock_guard<std::mutex> guard(componentsMutex);
		for(auto& [poolIndex, components] : components){
			for(const auto& component : components) {
				delete component;
			} components.clear();
		} components.clear();
	}

	{
		std::lock_guard<std::mutex> guard(entitiesMutex);
		entities.clear();
	}
}

EntityHandle ECS::CreateEntity()
{
	auto newEntity = std::make_unique<EntityRawType>();
	EntityHandle handle = (EntityHandle)newEntity.get();

	newEntity->first = entities.size();

	{
		std::lock_guard<std::mutex> guard(entitiesMutex);
		entities.push_back(std::move(newEntity));
	}

	return handle;
}

bool ECS::DestroyEntity(EntityHandle handle)
{
	const Entity& entityComponents = HandleToEntity(handle);
	for(const auto [componentID, index] : entityComponents) {
		DeleteComponent(componentID, index);
	}

	const size_t indexToRemove = HandleToEntityIndex(handle);
	{
		std::lock_guard<std::mutex> guard(entitiesMutex);

		const size_t lastIndex = entities.size() - 1;
		entities[indexToRemove].reset();
		entities[indexToRemove] = std::move(entities[lastIndex]);
		entities.pop_back();
	}

	return true;
}

void ECS::Update(ECSSystemList& systems, Timestep deltaTime)
{
	//TODO: better naming ASAP, because my eyes bleed!!!!!
	std::vector<std::vector<BaseECSComponent*>*> componentVectors;
	std::vector<BaseECSComponent*> componentsToUpdate;
	for(const auto& system : systems) {
		std::vector<size_t> componentTypes;

		{
			std::lock_guard<std::mutex> guard(system->componentTypeMutex);
			componentTypes = system->componentTypes;
		}

		if(componentTypes.size() == 1U) {
			std::lock_guard<std::mutex> guard(componentsMutex);
			const auto& component = components.at(componentTypes.at(0));
			for(BaseECSComponent* c : component) {
				system->Update(deltaTime, &c);
			}
		} else {
			componentsToUpdate.resize(std::max(componentsToUpdate.size(), componentTypes.size()));
			componentVectors.resize(std::max(componentVectors.size(), componentTypes.size()));

			for(size_t i = 0; i < componentTypes.size(); i++) {
				std::lock_guard<std::mutex> guard(componentsMutex);
				componentVectors[i] = &components[componentTypes[i]];
			}

			const size_t minSizeIndex = FindLeastCommonComponent(componentTypes);

			const auto& c = *componentVectors.at(minSizeIndex);
			for(const auto& c2 : c) {
				componentsToUpdate[minSizeIndex] = c2;

				Entity& entityComponents = HandleToEntity(componentsToUpdate.at(minSizeIndex)->entity);

				bool isValid = true;
				for(size_t j = 0; j < componentTypes.size(); j++) {
					if(j == minSizeIndex) {
						continue;
					}

					componentsToUpdate[j] = getComponentInternal(entityComponents, *componentVectors[j], componentTypes[j]);
					if(!componentsToUpdate.at(j)) {
						isValid = false;
						break;
					}
				}

				if(isValid) {
					system->Update(deltaTime, &componentsToUpdate[0]);
				}
			}
		}
	}
}

bool ECS::RemoveComponentInternal(EntityHandle handle, size_t componentID)
{
	Entity& entityComponents = HandleToEntity(handle);
	for(size_t i = 0; i < entityComponents.size(); i++) {
		const auto [ID, index] = entityComponents.at(i);
		if(componentID == ID) {
			DeleteComponent(ID, index);

			const size_t indexToRemove = i;
			const size_t lastIndex = entityComponents.size() - 1;
			entityComponents[indexToRemove] = entityComponents[lastIndex];
			entityComponents.pop_back();

			return true;
		}
	} return false;
}

BaseECSComponent* ECS::getComponentInternal(Entity& entityComponents, std::vector<BaseECSComponent*>& vector, size_t componentID)
{
	for(const auto [ID, index] : entityComponents) {
		if(ID == componentID) {
			return vector[index];
		}
	} return nullptr;
}

void ECS::DeleteComponent(size_t componentID, size_t index)
{
	std::lock_guard<std::mutex> guard(componentsMutex);
	auto& componentsPool = components[componentID];

	const size_t lastIndex = componentsPool.size() - 1;

	if(index == lastIndex) {
		componentsPool.pop_back();
		return;
	}

	const auto lastComponent = componentsPool.back();
	const size_t componentToRemove = index;
	componentsPool[componentToRemove] = componentsPool[lastIndex];

	Entity& entityComponents = HandleToEntity(lastComponent->entity);
	for(auto& [ID, componentIndex] : entityComponents) {
		if(componentID == ID && lastIndex == componentIndex) {
			componentIndex = index;
			break;
		}
	}

	componentsPool.pop_back();
}

size_t ECS::FindLeastCommonComponent(const std::vector<size_t>& componentTypes)
{
	size_t minSize = -1;
	size_t minIndex = -1;
	for(size_t i = 0; i < componentTypes.size(); i++) {
		const size_t size = -1;
		{
			std::lock_guard<std::mutex> guard(componentsMutex);
			components.at(componentTypes.at(i)).size();
		}
		if(size <= minSize) {
			minSize = size;
			minIndex = i;
		}
	}
	return minIndex;
}