#pragma once

#include <memory>
#include <unordered_map>

#include "ECSSystem.h"

//TODO: consider implementing lock-free
struct ECS
{
	ECS() = default;
	~ECS();

	EntityHandle CreateEntity();
	bool DestroyEntity(EntityHandle handle);

	void Update(ECSSystemList& systems, [[maybe_unused]] Timestep deltaTime);

	template<typename Component, typename ... Args>
	inline Component* AddComponent(EntityHandle entity, Args&& ... args)
	{
		std::lock_guard<std::mutex> guard(componentsMutex);
		components[Component::ID].push_back(new Component(std::forward<Args>(args)...));
		Component* newComponent = (Component*)components[Component::ID].back();
		newComponent->entity = entity;
		HandleToEntity(entity).emplace_back(Component::ID, components.at(Component::ID).size() - 1);

		return newComponent;
	}

	template<typename Component>
	bool RemoveComponent(EntityHandle entity)
	{
		return RemoveComponentInternal(entity, Component::ID);
	}

	template<typename Component>
	[[nodiscard]] Component* getComponent(EntityHandle entity)
	{
		std::lock_guard<std::mutex> guard(componentsMutex);
		return (Component*)getComponentInternal(HandleToEntity(entity), components[Component::ID], Component::ID);
	}

	template<typename Func>
	void each(Func func)
	{
		for(const auto& entityRaw : entities) {
			EntityHandle* entity = (EntityHandle*)entityRaw.get();
			func(entity);
		}
	}

	//TODO: convert it somehow to vector of T not of BaseECSComponent
	template<typename T>
	std::vector<BaseECSComponent*>& getComponentsOfTypeT()
	{
		std::lock_guard<std::mutex> guard(componentsMutex);
		return components[T::ID];
	}

private:
	ECS(const ECS& other) = delete;
	ECS& operator=(const ECS& other) = delete;

	typedef std::vector<std::pair<size_t, size_t>> Entity;
	typedef std::pair<size_t, Entity> EntityRawType;

	bool RemoveComponentInternal(EntityHandle handle, size_t componentID);
	void DeleteComponent(size_t componentID, size_t index);

	[[nodiscard]] BaseECSComponent* getComponentInternal(Entity& entityComponents, std::vector<BaseECSComponent*>& vector, size_t componentID);

	[[nodiscard]] size_t FindLeastCommonComponent(const std::vector<size_t>& componentTypes);

	[[nodiscard]] inline EntityRawType* HandleToRawType(EntityHandle handle) const { return (EntityRawType*)handle; }
	[[nodiscard]] inline size_t HandleToEntityIndex(EntityHandle handle) const { return HandleToRawType(handle)->first; }
	[[nodiscard]] inline Entity& HandleToEntity(EntityHandle handle) const { return HandleToRawType(handle)->second; }

	std::mutex entitiesMutex;
	std::vector<std::unique_ptr<EntityRawType>> entities;

	std::mutex componentsMutex;
	std::unordered_map<size_t, std::vector<BaseECSComponent*>> components;
};
